package ru.nlmk.study.jse49.ex3;

public interface Downloader {
    String download(String path);
}
