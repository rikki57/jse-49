package ru.nlmk.study.jse49.ex3;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppContext {

    @Bean
    public Downloader downloader() {
        return new FileDownloader();
    }

    @Bean
    public Uploader uploader() {
        return new FileUploader();
    }

    @Bean
    public DataHandler dataHandler() {
        return new DataHandler(downloader(), uploader());
    }
}
