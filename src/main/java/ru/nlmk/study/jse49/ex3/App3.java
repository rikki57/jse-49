package ru.nlmk.study.jse49.ex3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App3 {
    public static void main(String[] args) {
        //создаем IoC контейнер спринга
        ApplicationContext ctx =
               new AnnotationConfigApplicationContext(AppContext.class);

        // запросили бин из контекста
        DataHandler dataHander = (DataHandler) ctx.getBean("dataHandler");

        dataHander.handleData("/data/office/files", "/data/office/bank");

    }
}
