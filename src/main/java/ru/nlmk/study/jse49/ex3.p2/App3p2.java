package ru.nlmk.study.jse49.ex3.p2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App3p2 {
    public static void main(String[] args) {
        //создаем IoC контейнер спринга
        ApplicationContext ctx =
               new AnnotationConfigApplicationContext(AppContext.class);

        // запросили бин из контекста
        DataHandler dataHander = (DataHandler) ctx.getBean("dataHandler");

        dataHander.handleData("/data/office/files", "/data/office/bank");

    }
}
