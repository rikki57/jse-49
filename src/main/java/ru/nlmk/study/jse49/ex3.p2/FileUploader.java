package ru.nlmk.study.jse49.ex3.p2;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class FileUploader implements Uploader {
    @Override
    public boolean upload(String path, Object data) {
        System.out.println("Upload to: " + path);
        return true;
    }
}
