package ru.nlmk.study.jse49.ex3.p2;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Qualifier("email-market-downloader")
public class EmailDownloader implements Downloader {

    @Override
    public String download(String path) {
        System.out.println("Download email:" + path);
        return "email-content";
    }
}
