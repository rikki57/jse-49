package ru.nlmk.study.jse49.ex3.p2;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
@Qualifier("file-business-downloader")
public class FileDownloader implements Downloader {
    @Override
    public String download(String path) {
        System.out.println("Download from:" + path);
        return "информация";
    }
}
