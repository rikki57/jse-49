package ru.nlmk.study.jse49.ex3.p2;

public interface Uploader {
    boolean upload(String path, Object data);
}
