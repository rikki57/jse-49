package ru.nlmk.study.jse49.ex3.p2;

public interface Downloader {
    String download(String path);
}
