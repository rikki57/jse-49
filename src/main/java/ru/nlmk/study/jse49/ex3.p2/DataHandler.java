package ru.nlmk.study.jse49.ex3.p2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class DataHandler {

    private Downloader downloader;
    private Uploader uploader;

    @Autowired
    public DataHandler(Downloader downloader, Uploader uploader) {
        this.downloader = downloader;
        this.uploader = uploader;
    }

    public void handleData(String fromPath, String toPath) {
        String result = downloader.download(fromPath);
        seriousLogic(result);
        uploader.upload(toPath, result);
    }

    private void seriousLogic(String data) {
        System.out.println("Processing data: " + data);
        System.out.println("Done!");
    }
}
