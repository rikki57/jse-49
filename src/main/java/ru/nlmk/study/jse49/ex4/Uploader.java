package ru.nlmk.study.jse49.ex4;

public interface Uploader {
    boolean upload(String path, Object data);
}
