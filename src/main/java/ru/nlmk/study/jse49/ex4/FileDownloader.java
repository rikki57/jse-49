package ru.nlmk.study.jse49.ex4;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class FileDownloader implements Downloader {
    @Override
    public String download(String path) {
        System.out.println("Download from:" + path);
        return "информация";
    }
}
