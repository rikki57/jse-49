package ru.nlmk.study.jse49.ex4;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App4 {
    public static void main(String[] args) {
        //создаем IoC контейнер спринга
        ApplicationContext ctx =
               new AnnotationConfigApplicationContext(AppContext.class);

        // singleton
        DataHandler dh1 = (DataHandler) ctx.getBean("dataHandler");

        DataHandler dh2 = (DataHandler) ctx.getBean("dataHandler");

        DataHandler dh3 = (DataHandler) ctx.getBean("dataHandler");

        System.out.println(dh1 + " " + dh2 + " " + dh3);

        System.out.println(dh1 == dh2);
        System.out.println(dh2 == dh3);

//        dataHander.handleData("/data/office/files", "/data/office/bank");

        FileDownloader f1 =(FileDownloader) ctx.getBean("fileDownloader");

        FileDownloader f2 =(FileDownloader) ctx.getBean("fileDownloader");

        FileDownloader f3 =(FileDownloader) ctx.getBean("fileDownloader");

        System.out.println(f1 + " " + f2 + " " + f3);

        System.out.println(f1 == f2);
        System.out.println(f2 == f3);
    }
}
