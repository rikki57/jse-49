package ru.nlmk.study.jse49.ex4;

import org.springframework.stereotype.Component;

@Component
public class FileUploader implements Uploader {
    @Override
    public boolean upload(String path, Object data) {
        System.out.println("Upload to: " + path);
        return true;
    }
}
