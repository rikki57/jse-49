package ru.nlmk.study.jse49.ex4;

public interface Downloader {
    String download(String path);
}
