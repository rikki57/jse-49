package ru.nlmk.study.jse49.ex2;

public interface Downloader {
    String download(String path);
}
