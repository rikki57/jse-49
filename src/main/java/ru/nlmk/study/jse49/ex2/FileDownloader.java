package ru.nlmk.study.jse49.ex2;

import org.springframework.stereotype.Component;

@Component
public class FileDownloader implements Downloader {
    @Override
    public String download(String path) {
        System.out.println("Download from:" + path);
        return "информация";
    }
}
