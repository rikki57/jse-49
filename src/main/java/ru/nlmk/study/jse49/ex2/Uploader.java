package ru.nlmk.study.jse49.ex2;

public interface Uploader {
    boolean upload(String path, Object data);
}
