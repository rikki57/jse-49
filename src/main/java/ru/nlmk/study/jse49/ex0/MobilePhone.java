package ru.nlmk.study.jse49.ex0;

public class MobilePhone {

    private Battery battery;
    private Screen screen;

    // правильно!
//    public MobilePhone(Battery battery, Screen screen) {
//        this.battery = battery;
//        this.screen = screen;
//    }

    // неправильно!
    public MobilePhone() {
        this.battery = new Battery();
        this.screen = new Screen();
    }

    public void doSomething() {
        // business logic
    }
}
