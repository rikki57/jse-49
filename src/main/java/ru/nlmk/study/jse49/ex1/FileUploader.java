package ru.nlmk.study.jse49.ex1;

public class FileUploader implements Uploader{
    @Override
    public boolean upload(String path, Object data) {
        System.out.println("Upload to: " + path);
        return true;
    }
}
