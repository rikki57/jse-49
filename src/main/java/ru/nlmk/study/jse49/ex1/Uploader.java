package ru.nlmk.study.jse49.ex1;

public interface Uploader {
    boolean upload(String path, Object data);
}
