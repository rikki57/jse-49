package ru.nlmk.study.jse49.ex1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App1 {
    public static void main(String[] args) {
        //создаем IoC контейнер спринга
        ApplicationContext ctx =
               new ClassPathXmlApplicationContext("ex1.xml");

        // запросили бин из контекста
        DataHandler dataHander = (DataHandler) ctx.getBean("dataHander");

        dataHander.handleData("/data/office/files", "/data/office/bank");

    }
}
