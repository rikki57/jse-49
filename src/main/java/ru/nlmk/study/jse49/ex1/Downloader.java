package ru.nlmk.study.jse49.ex1;

public interface Downloader {
    String download(String path);
}
